import Vue from 'vue';
import Router from 'vue-router';
import Lobby from './views/Lobby.vue';

Vue.use(Router);

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'lobby',
            component: Lobby,
        },
        {
            path: '/:roomName',
            name: 'game',
            component: () => import(/* webpackChunkName: "game" */ './views/Game.vue'),
            props: true,
        },
        // {
        //     path: '/about',
        //     name: 'about',
        //     // route level code-splitting
        //     // this generates a separate chunk (about.[hash].js) for this route
        //     // which is lazy-loaded when the route is visited.
        //     component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
        // },
    ],
});
