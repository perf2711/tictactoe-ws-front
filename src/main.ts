import Vue from 'vue';
import App from './App.vue';
import router from './router';
import { store } from './store';
import { GameRequestHandler } from './modules/gameModule';

Vue.config.productionTip = false;

store.state.requestHandlers.push(new GameRequestHandler(store));

new Vue({
    router,
    store,
    render: (h) => h(App),
}).$mount('#app');
