import Vuex, { GetterTree, MutationTree, ActionTree, Store } from 'vuex';
import { communicationModule, ICommunicationModuleState } from './modules/communicationModule';
import { IRequestHandler } from './modules/communicationModels';
import Vue from 'vue';
import { lobbyModule, ILobbyModuleState } from './modules/lobbyModule';
import { gameModule, IGameModuleState } from './modules/gameModule';

Vue.use(Vuex);

export interface IRootState {
    requestHandlers: IRequestHandler[];

    communication?: ICommunicationModuleState;
    lobby?: ILobbyModuleState;
    game?: IGameModuleState;
}

const getters: GetterTree<IRootState, IRootState> = {

};

const mutations: MutationTree<IRootState> = {
    addRequestHandler(state, requestHandler: IRequestHandler) {
        state.requestHandlers.push(requestHandler);
    },
};

const actions: ActionTree<IRootState, IRootState> = {
    addRequestHandler({commit}, requestHandler: IRequestHandler) {
        commit('addRequestHandler', requestHandler);
    },
};

export const store = new Store<IRootState>({
    actions,
    getters,
    mutations,
    modules: {
        communication: communicationModule,
        lobby: lobbyModule,
        game: gameModule,
    },
    state: {
        requestHandlers: [],
    },
});
