import { Module, GetterTree, MutationTree, ActionTree } from 'vuex';
import { IRootState } from '@/store';
import { IPendingRequest, IRequest, IReply } from './communicationModels';
import Vue from 'vue';

export interface ICommunicationModuleState {
    hostname: string;
    websocket?: WebSocket;
    deferredRequests: IRequest[];
    pendingRequests: {[key: string]: IPendingRequest};
}

const getters: GetterTree<ICommunicationModuleState, IRootState> = {

};

const mutations: MutationTree<ICommunicationModuleState> = {
    setWebSocket(state, websocket: WebSocket) {
        Vue.set(state, 'websocket', websocket);
    },
    addRequest(state, pendingRequest: IPendingRequest) {
        if (pendingRequest.request.id) {
            state.pendingRequests[pendingRequest.request.id] = pendingRequest;
        }
    },
    deferRequest(state, request: IRequest) {
        state.deferredRequests.push(request);
    },
};

const actions: ActionTree<ICommunicationModuleState, IRootState> = {
    connect({commit, dispatch}, hostname: string) {
        return new Promise<void>((resolve, reject) => {
            const websocket = new WebSocket(hostname);

            websocket.onopen = () => {
                commit('setWebSocket', websocket);
                dispatch('sendDeferredRequests');
                resolve();
            };

            websocket.onerror = () => {
                reject();
            };

            websocket.onmessage = (ev) => {
                dispatch('processMessage', JSON.parse(ev.data));
            };
        });
    },
    disconnect({commit, state}) {
        if (state.websocket) {
            state.websocket.close();
            commit('setWebSocket', undefined);
        }
    },
    sendRequest({commit, state}, request: IRequest) {
        const pendingRequest: IPendingRequest = {
            request,
        };

        const promise = new Promise<IReply>((resolve, reject) => {
            pendingRequest.resolve = resolve;
            pendingRequest.reject = reject;
        });

        request.id = (Math.random() * 1e10).toFixed(0).toString();
        commit('addRequest', pendingRequest);

        if (!state.websocket) {
            commit('deferRequest', request);
            console.log('Deferring request', request);
        } else {
            state.websocket.send(JSON.stringify(request));
            console.log('Sent request', request);
        }

        return promise;
    },
    sendReply({state}, payload: {request: IRequest, success: boolean, data: any}) {
        if (!state.websocket) {
            return;
        }

        const reply: IReply = {
            id: payload.request.id,
            message: payload.data,
            success: payload.success,
        };
        state.websocket.send(JSON.stringify(reply));
        console.log('Sent reply', reply);
    },
    processMessage({dispatch, state, rootState}, message: IRequest | IReply) {
        console.log('New message', message);
        if ('success' in message && message.id) {
            const pendingRequest = state.pendingRequests[message.id];
            if (!pendingRequest) {
                return;
            }

            delete state.pendingRequests[message.id];

            if (message.success && pendingRequest.resolve) {
                pendingRequest.resolve(message);
            } else if (!message.success && pendingRequest.reject) {
                pendingRequest.reject(message);
            }
        } else if ('type' in message) {
            for (const handler of rootState.requestHandlers) {
                if (handler.handle(
                        message,
                        (data) => dispatch('sendReply', {request: message, success: true, data}),
                        (data) => dispatch('sendReply', {request: message, success: false, data}),
                    )) {
                    break;
                }
            }
        }
    },
    sendDeferredRequests({state}) {
        if (!state.websocket) {
            return;
        }

        for (const request of state.deferredRequests) {
            state.websocket.send(JSON.stringify(request));
            console.log('Sent deferred request', request);
        }
        state.deferredRequests = [];
    },
};

export const communicationModule: Module<ICommunicationModuleState, IRootState> = {
    actions,
    getters,
    mutations,
    state: {
        hostname: 'ws://10.21.92.20:8008/',
        deferredRequests: [],
        pendingRequests: {},
    },
};
