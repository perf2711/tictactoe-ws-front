import { GetterTree, MutationTree, ActionTree, Module } from 'vuex';
import { IRootState } from '@/store';
import { IReply } from './communicationModels';
import Vue from 'vue';

export interface ILobbyModuleState {
    rooms: string[];
}

const getters: GetterTree<ILobbyModuleState, IRootState> = {

};

const mutations: MutationTree<ILobbyModuleState> = {
    setRooms(state, roomList) {
        Vue.set(state, 'rooms', roomList);
    },
};

const actions: ActionTree<ILobbyModuleState, IRootState> = {
    async getRooms({dispatch, commit}) {
        const reply = await dispatch('sendRequest', {
             type: 'ListRooms',
        }, {root: true}) as IReply<string[]>;
        commit('setRooms', reply.message);
    },
    async joinRoom({dispatch}, room) {
        const reply = await dispatch('sendRequest', {
            type: 'JoinRoom',
            data: room,
        }, {root: true}) as IReply<number>;

        await dispatch('game/initGame', reply.message, {root: true});
    },
};

export const lobbyModule: Module<ILobbyModuleState, IRootState> = {
    actions,
    getters,
    mutations,
    namespaced: true,
    state: {
        rooms: [],
    },
};
