export interface IRequest<T = any> {
    id?: string;
    type: 'JoinRoom' | 'ListRooms' | 'GameStart' | 'Move',
    data?: T;
}

export interface IReply<T = any> {
    id?: string;
    success: boolean;
    message: T;
}

export interface IPendingRequest {
    request: IRequest;

    resolve?(reply: IReply): void;
    reject?(reason?: IReply | any): void;
}

export interface IClientOptions {
    onMessage: (data: IRequest) => void;
}

export interface ICaller {
    request(data: IRequest): Promise<IReply>;
}

export interface IRequestHandler {
    handle(request: IRequest, resolve: (data: any) => void, reject: (data: any) => void): Promise<boolean>;
}
