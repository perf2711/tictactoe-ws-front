import { GetterTree, MutationTree, ActionTree, Module, Store } from 'vuex';
import { IRootState } from '@/store';
import Vue from 'vue';
import { IRequestHandler, IRequest, IReply } from './communicationModels';

export interface IGameModuleState {
    selfPlayerId?: number;
    currentPlayerId?: number;
    winningPlayerId?: number;
    map: {[key: number]: {[key: number]: number}};
}

const getters: GetterTree<IGameModuleState, IRootState> = {

};

const mutations: MutationTree<IGameModuleState> = {
    setSelfId(state, id) {
        Vue.set(state, 'selfPlayerId', id);
    },
    setCurrentPlayerId(state, id) {
        Vue.set(state, 'currentPlayerId', id);
    },
    setMapMove(state, {column, row, playerId}) {
        state.map[column][row] = playerId;
    },
    resetMap(state) {
        Vue.set(state, 'map', {
            0: {
                0: 0,
                1: 0,
                2: 0,
            },
            1: {
                0: 0,
                1: 0,
                2: 0,
            },
            2: {
                0: 0,
                1: 0,
                2: 0,
            },
        });
    },
    setNextPlayer(state) {
        Vue.set(state, 'currentPlayerId', state.currentPlayerId === 1 ? 2 : 1);
    },
    setWinningPlayer(state, playerId) {
        Vue.set(state, 'winningPlayerId', playerId);
    }
};

const actions: ActionTree<IGameModuleState, IRootState> = {
    initGame({commit}, playerId: number) {
        commit('setSelfId', playerId);
    },
    startGame({commit}, startingPlayerId: number) {
        commit('resetMap');
        commit('setCurrentPlayerId', startingPlayerId);
    },
    async makeMove({commit, state, dispatch}, {column, row}) {
        if (state.currentPlayerId !== state.selfPlayerId) {
            return Promise.resolve();
        }

        await dispatch('sendRequest', {
            type: 'Move',
            data: {x: column, y: row},
        }, {root: true});

        commit('setMapMove', { column, row, playerId: state.selfPlayerId});
        commit('setNextPlayer');
    },
    makeOpponentMove({commit, state}, {column, row}) {
        const playerId = state.selfPlayerId === 1 ? 2 : 1;
        commit('setMapMove', { column, row, playerId });
        commit('setNextPlayer');
    },
    endGame({commit}, winningPlayerId) {
        commit('setWinningPlayer', winningPlayerId);
    },
    async newGame({dispatch}) {
        const reply = await dispatch('sendRequest', {
            type: 'GameStart',
        }, {root: true}) as IReply<number>;
        dispatch('startGame', reply.message);
    }
};

export class GameRequestHandler implements IRequestHandler {
    constructor(private store: Store<IRootState>) {}

    public async handle(
        request: IRequest<any>,
        resolve: (data: any) => void,
        reject: (data: any) => void): Promise<boolean> {
        try {
            switch (request.type) {
                case 'GameStart':
                    resolve(await this.store.dispatch('game/startGame', request.data));
                    break;
                case 'Move':
                    resolve(await this.store.dispatch('game/makeOpponentMove', {
                        column: request.data.x,
                        row: request.data.y,
                    }));
                    break;
                case 'GameEnd':
                    resolve(await this.store.dispatch('game/endGame', request.data));
                    break;
                default:
                    return false;
            }
        } catch (err) {
            reject(err);
        }

        return true;
    }
}

export const gameModule: Module<IGameModuleState, IRootState> = {
    actions,
    getters,
    mutations,
    namespaced: true,
    state: {
        map: {
            0: {
                0: 0,
                1: 0,
                2: 0,
            },
            1: {
                0: 0,
                1: 0,
                2: 0,
            },
            2: {
                0: 0,
                1: 0,
                2: 0,
            },
        },
    },
};
